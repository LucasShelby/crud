<?php
    include_once(__DIR__."../class/usuario.php");

    $id = $_GET['id'];

    $usuario = new usuario();
    $item = $usuario->item($id);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="action/usuario.php" method="POST" role="form">
        <legend>Usuarios</legend>
    
        <div class="form-group">
            <br>
            <label for="">Nome</label>
            <input name="edNome" type="text" class="form-control" id="" value="<?php echo $item[0]['nome'] ?>" placeholder="Nome de Usuario">
        </div>
        <div class="form-group">
            <br>
            <label for="">E-mail</label>
            <input name="edEmail" type="text" class="form-control" id="" value="<?php echo $item[0]['email'] ?>" placeholder="E-mail">
        </div>
        <div class="form-group">
            <br>
            <label for="">Telefone</label>
            <input name="edFone" type="text" class="form-control" id="" value="<?php echo $item[0]['fone'] ?>" placeholder="Telefone">
        </div>
        <div class="form-group">
            <br>
            <label for="">Senha</label>
            <input name="edSenha" type="text" class="form-control" id="" value="<?php echo $item[0]['senha'] ?>" placeholder="Senha para login">
        </div>
        <input type="hidden" name="action" value="ed">
        <input type="hidden" name="id" value="<?php echo $item[0]['id'] ?>">
    
        <br>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
    <br>
    <button onclick="window.location.href='lista.php'" class="btn btn-primary">LISTA</button>            

</body>
</html>