<?php 
require_once(__DIR__."/banco.php");

class usuario
{
    function __construct()
    {
        $this->banco = new banco();
        $this->tabela = 'crud2';
    }

    function cadastrar($dados)
    {
        if (!is_array($dados)) {
            return false;
        } 
        

        $result = $this->banco->insert($this->tabela,$dados);
        return $result;
    }

    function editar($dados,$campoWhere)
    {
        if(!is_array($dados)) {
            return false;
        }

        $result = $this->banco->update($this->tabela,$dados,$campoWhere);
        return $result;
    }

    function item ($id)
    {
        $result = $this->banco->select($this->tabela,array ('id'=>$id));
        return $result;
    }

    function listar()
    {
        $result = $this->banco->select($this->tabela);
        return $result;
    }
    
    function desativa($campoWhere)
    {
        $result = $this->banco->desativa($this->tabela,$campoWhere);
        return $result;
    }

    function ativa($campoWhere)
    {
        $result = $this->banco->ativa($this->tabela,$campoWhere);
    }



}

?>