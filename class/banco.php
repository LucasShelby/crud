<?php

class banco {
    function __construct()
    {
        $this->connect = $this->connect(); 
    }

    function connect()
    {
        $connect = mysqli_connect('127.0.0.1','root','','treino');
        return $connect;
    }

    function query($query)
    {
        try {
            if ($this->connect === false) {
                throw new Exception("Erro ao conectar no Banco");
            }

            $result = mysqli_query($this->connect,$query);
            return $result;

        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    function insert($tabela,$dados)
        //INSERT INTO produtos VALUES (“Exemplo”, 1, “Produto exemplo”);
    {
        $query = "INSERT INTO ".$tabela;

        $campos = "(";
        $valores = " VALUES(";

        foreach ($dados as $key=>$value) {
            $campos.= $key.',';
            $valores.= $this->isNumeric($value).',';
        }

        $campos = $this->removeVirgula($campos);
        $valores = $this->removeVirgula($valores);

        $campos .= ")";
        $valores .= ")";

        $query .= $campos.$valores;
        $result = $this->query($query);
        return $result;

    }

    function update($tabela,$dados,$campoWhere)
        //UPDATE produtos SET descricao = “Produto exemplo 2” WHERE codigo = 2;
    {
        $query = "UPDATE ".$tabela." SET ";

        $valores = '';

        foreach ($dados as $key=>$value) {
            $valores.= ' '.$key.'='.$this->isNumeric($value).',';
        }

        $valores = $this->removeVirgula($valores);

        $query.=$valores." WHERE ".$this->make_where($campoWhere);

        $result = $this->query($query);
        return $result;
    }

    function select($tabela,$campoWhere=null)
        //SELECT * FROM produtos;
    {
        $query = "SELECT * FROM ".$tabela."";

        if (is_array($campoWhere)) {
            $query .= ' WHERE '.$this->make_where($campoWhere);
        } elseif (is_string($campoWhere)) {
            $query .= ' WHERE '.$campoWhere;
        }

        $result = $this->query($query);
        if(!$result) {
            return $result;
        }
        while ($linha = mysqli_fetch_assoc($result)) {
            $linhas[] = $linha;                
        }
        return $linhas;
    }

    function make_where ($array)
    {
        if (!is_array($array)) {
            return false;
        }

        $where = '';
        foreach ($array as $key=>$value) {
            $key = explode (' ',$key);
            if ($where != '') {
                $where.= ' and ';
            }
            $where.= $key[0].(isset($key[1]) ? $key[1] : '=').$this->isNumeric($value);
            return $where;
        }
    }

    function isNumeric($value)
    {
        if (!is_numeric($value)&&!is_null($value)) {
            $value = "'".$value."'";
        }
        return $value;
    }

    function removeVirgula($string)
    {
        return substr($string,0, strlen($string)-1);
    }

    function desativa($tabela,$campoWhere,$soft=true)
    {
        if ($soft) {
            $query = "UPDATE ".$tabela." SET ativo=0";
        } else {
            $query = "DELETE FROM".$tabela;
        }
        $query .= " WHERE ".$this->make_where($campoWhere);

        $result = $this->query($query);
        return $result;
    }

    function ativa($tabela,$campoWhere)
    {
        $query = "UPDATE ".$tabela." SET ativo=1"." WHERE ".$this->make_where($campoWhere);

        $result = $this->query($query);
        return $result;
    }
}   

?>

