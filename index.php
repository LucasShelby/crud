<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="action/usuario.php" method="POST" role="form">
        <legend>Usuarios</legend>
    
        <div class="form-group">
            <br>
            <label for="">Nome</label>
            <input name="nome" type="text" class="form-control" id="" placeholder="Nome de Usuario">
        </div>
        <div class="form-group">
            <br>
            <label for="">E-mail</label>
            <input name="email" type="text" class="form-control" id="" placeholder="E-mail">
        </div>
        <div class="form-group">
            <br>
            <label for="">Telefone</label>
            <input name="fone" type="text" class="form-control" id="" placeholder="Telefone">
        </div>
        <div class="form-group">
            <br>
            <label for="">Senha</label>
            <input name="senha" type="text" class="form-control" id="" placeholder="Senha para login">
        </div>
        <input type="hidden" name="action" value="new">
    
        <br>
        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
    <br>
    <button onclick="window.location.href='lista.php'" class="btn btn-primary">LISTA</button>            

</body>
</html>