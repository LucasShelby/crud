<?php
    include_once(__DIR__.'../class/usuario.php');
    $usuario = new usuario;

    $dado = $usuario->listar();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table border="1" align="center">
        <br><br><br><br><tr>
            <th>Num. ID</th>
            <th>Nome</th>
            <th>E-Mail</th>
            <th>Telefone</th>
            <th>Senha</th>
            <th>Ativo</th>
            <th>Ação</th>
        </tr>
        <?php  foreach ($dado as $key=>$value) { ?>
        <tr>
            <td><?php echo $value['id']; ?></td>
            <td><?php echo $value['nome']; ?></td>
            <td><?php echo $value['email']; ?></td>
            <td><?php echo $value['fone']; ?></td>
            <td><?php echo $value['senha']; ?></td>
            <td><?php echo $value['ativo']==1 ? 'SIM' : 'NÃO'; ?></td>
            <td>
                <button id="editar" value="2" onclick="window.location.href='editar.php?id=<?php echo $value['id']; ?>'" class="btn btn-primary" >EDITAR</button>
                <button id="desativar" value="3" onclick="window.location.href='action/usuario.php?id=<?php echo $value['id'].($value['ativo'] == 1 ?'&action=off' : '&action=on') ?>'" class="btn btn-primary" ><?php echo $value['ativo']==1 ? "DESATIVAR" : "ATIVAR"; ?></button>
            </td>                        
        </tr>
    <?php } ?>

    </table>
</body>
</html>