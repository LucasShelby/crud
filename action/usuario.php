<?php
    require_once(__DIR__."/../class/banco.php");
    //require_once(__DIR__."/../helpers/action.php");
    require_once(__DIR__."/../class/usuario.php");

    $usuario = new usuario;
    $banco = new banco;

    if (isset($_POST['id'])||isset($_GET['id'])) {
        $where = array(
            'id'=>(isset($_POST['id']) ? $_POST['id'] : $_GET['id']),
        );
    }

    $action = isset($_POST['action']) ? $_POST['action'] : $_GET['action'];

    switch ($action) {
        case 'new';
            if (isset($_POST['nome'])) {
                $campos = array(
                    'nome'=>'nome',
                    'email'=>'email',
                    'fone'=>'fone',
                    'senha'=>'senha',
                );

                foreach ($campos as $key=>$value) {
                    if ($_POST[$key] == '') {
                        echo "PREENCHA ".$value."!";               }
                }
            } else {
                echo "Erro ao Cadastrar no BD";
                exit;
            }

            if (isset($_POST['action'])) {
                 $dados = array(
                'nome' => $_POST['nome'],
                'email' => $_POST['email'],
                'fone' => $_POST['fone'],
                'senha' => $_POST['senha'],
            );
            }
           
            $result = $usuario->cadastrar($dados);
            if ($result) {
                echo "CADASTRADO com SUCESSO";
            } else {
                echo "ERRO ao CADASTRAR";
            }

        break;
        
        case 'ed';
            if (isset($_POST['edNome'])) {
               $campos = array (
                    'edNome'=>'edNome',
                    'edEmail'=>'edEmail',
                    'edFone'=>'edFone',
                    'edSenha'=>'edSenha',
                );
                foreach ($campos as $key=>$value) {
                    if ($_POST[$key] == '') {
                        echo 'Preencha '.$value.'!';
                    }
                }
            } else {
                echo "Erro ao Editar!";
                exit;
            }

            $dados = array (
                'nome'=>$_POST['edNome'],
                'email'=>$_POST['edEmail'],
                'fone'=>$_POST['edFone'],
                'senha'=>$_POST['edSenha'],
            );

            $result = $usuario->editar($dados,$where);
            if ($result) {
                echo "Editado com Sucesso";
            } else {
                echo "Erro ao editar, verifique os dados";
            }

        break;
        
        case 'off';
            if (isset($where['id'])) {
                if ($usuario->desativa($where)) {
                    echo "Item DESATIVADO!";
                }else {
                    echo "ERRO ao Desativar";
                }
            } 
        break;
        
        case 'on';
            if (isset($where['id'])) {
                if ($usuario->ativa($where)) {
                    echo "Item ATIVADO!";
                }else {
                    echo "ERRO ao Ativar";
                }
            }
        break;
    }

?>
<br><center>
<button onclick="window.location.href='../lista.php'" class="btn btn-primary">LISTA</button>
</center>